 #!/bin/bash

apt-get install -y gcc || exit 1

GO111MODULE=on GOCACHE=$(pwd)/gocache go/bin/go get sigs.k8s.io/kind@v0.5.1