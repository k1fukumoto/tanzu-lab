#!/bin/bash

apt-get update -y || exit 1
apt-get install -y gcc || exit 1
mkdir ./bin
mv /usr/bin/gcc ./bin/gcc || exit 1
