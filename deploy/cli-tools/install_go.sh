#!/bin/bash

apt-get update -y || exit 1
apt-get install -y wget || exit 1
wget https://dl.google.com/go/go1.13.linux-amd64.tar.gz || exit 1
tar -xzf go1.13.linux-amd64.tar.gz || exit 1

find / -name go