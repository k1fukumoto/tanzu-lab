#!/bin/sh

CLUSTER=$1

docker run --rm \
  -v "$(pwd)":/out \
  -v "$(pwd)/deploy/envvars.sh":/envvars.txt:ro \
  gcr.io/cluster-api-provider-vsphere/release/manifests:v0.5.1 \
  -c $CLUSTER

tmp=$(mktemp)
for f in $(ls out/$CLUSTER/*.yaml)
do
  echo $f
  sed $'s/sddc-cgw-horizon-cpsbu-desktops-1/\'sddc-cgw-horizon-cpsbu-desktops-1 \'/g' $f > $tmp
  echo $tmp ">" $f
  cp $tmp $f
done
rm $tmp

