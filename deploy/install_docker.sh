#!/bin/sh

export DEBIAN_FRONTEND=noninteractive 

apt-get -y update

apt-get install -y \
  apt-transport-https \
   ca-certificates \
   curl \
   gnupg-agent \
   software-properties-common || exit 1

(curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -) || exit 1

add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable" || exit 1

apt-get -y update || exit 1
apt-get -y remove docker-ce-cli || exit 1
apt-get -y install docker-ce docker-ce-cli containerd.io || exit 1

docker run hello-world
